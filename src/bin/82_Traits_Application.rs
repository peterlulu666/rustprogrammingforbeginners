trait Shape {
    fn perimeter(&self) -> f64;
}

struct Square {
    side: f64,
}

struct Triangle {
    a: f64,
    b: f64,
    c: f64,
}

impl Shape for Square {
    fn perimeter(&self) -> f64 {
        self.side * 4.0
    }
}

impl Shape for Triangle {
    fn perimeter(&self) -> f64 {
        self.a + self.b + self.c
    }
}


fn main() {
    let square = Square { side: 2.0 };
    let triangle = Triangle { a: 3.0, b: 4.0, c: 5.0 };
    println!("Square perimeter: {}", square.perimeter());
    println!("Triangle perimeter: {}", triangle.perimeter());
}