fn main() {
    let a = 100;
    if a > 300 {
        println!("Huge number ")
    } else if a > 200 && a < 300 {
        println!("Big number ")
    } else {
        println!("Small number ")
    }
}