trait Fall {
    fn hit_ground(&self);
}

struct Ball;

struct Cat;

impl Fall for Ball {
    fn hit_ground(&self) {
        println!("Ball hit the ground");
    }
}

impl Fall for Cat {
    fn hit_ground(&self) {
        println!("Cat hit the ground");
    }
}

fn print_fall(fall: &impl Fall) {
    fall.hit_ground();
}

fn main() {
    let ball = Ball;
    let cat = Cat;
    print_fall(&ball);
    print_fall(&cat);
}