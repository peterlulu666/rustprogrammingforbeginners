fn main() {
    let number = 10;
    if number < 10 {
        println!("number is less than 10");
    } else if number > 10 {
        println!("number is greater than 10");
    } else {
        println!("number is 10");
    }
}