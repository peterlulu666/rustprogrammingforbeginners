struct SoundData {
    sound: String,
}

impl SoundData {
    fn new(sound: String) -> Self {
        Self { sound }
    }
}

fn get_sound_data(name: &str) -> Result<SoundData, String> {
    if name == "alert" {
        Ok(SoundData::new("alert".to_string()))
    } else {
        Err("No sound data".to_string())
    }
}

fn main() {
    let sound = get_sound_data("alert");
    match sound {
        Ok(sound) => println!("sound: {}", sound.sound),
        Err(err) => println!("error: {}", err),
    }
}