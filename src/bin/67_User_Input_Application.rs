use std::io;

enum PowerState {
    Off,
    Sleep,
    Reboot,
    Shutdown,
    Hibernate,
}

fn print_state(power_state: PowerState) {
    match power_state {
        PowerState::Off => println!("The computer is off"),
        PowerState::Sleep => println!("The computer is sleeping"),
        PowerState::Reboot => println!("The computer is rebooting"),
        PowerState::Shutdown => println!("The computer is shutting down"),
        PowerState::Hibernate => println!("The computer is hibernating"),
    }
}

fn user_input() -> String {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read line");
    return input.trim().to_string();
}

fn main() {
    let mut input = user_input();
    input = input.to_uppercase();
    match input.as_str() {
        "OFF" => print_state(PowerState::Off),
        "SLEEP" => print_state(PowerState::Sleep),
        "REBOOT" => print_state(PowerState::Reboot),
        "SHUTDOWN" => print_state(PowerState::Shutdown),
        "HIBERNATE" => print_state(PowerState::Hibernate),
        _ => println!("Invalid input"),
    }
}