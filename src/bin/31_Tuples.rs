fn main() {
    let coord = (2, 3);
    println!("coord is {:?} {:?}", coord.0, coord.1);
    let (x, y) = (2, 3);
    println!("x is {:?} y is {:?}", x, y);
    let (name, age) = ("Wang", 18);
    println!("name is {:?} age is {:?}", name, age);
    let item = ("red", 10, "TX", "pizza", "TV SHOW", "home");
    let color = item.0;
    let state = item.2;
    println!("color is {:?} state is {:?}", color, state);
}