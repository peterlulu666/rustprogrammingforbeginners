enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn go_to(direction: Direction) -> String {
    match direction {
        Direction::Up => "Going up".to_owned(),
        Direction::Down => "Going down".to_owned(),
        Direction::Left => "Going left".to_owned(),
        Direction::Right => "Going right".to_owned(),
    }
}
fn main() {
    let g = go_to(Direction::Up);
    println!("{}", g);
}