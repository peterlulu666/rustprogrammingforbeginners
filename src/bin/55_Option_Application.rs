#[derive(Debug)]
struct Locker {
    name: String,
    number: Option<i32>,
}

fn main() {
    let lockers = vec![
        Locker {
            name: "Wang".to_string(),
            number: Some(1),
        },
        Locker {
            name: "Li".to_string(),
            number: Some(2),
        },
        Locker {
            name: "Tan".to_string(),
            number: None,
        },
    ];

    for locker in &lockers {
        println!("{}: {:?}", locker.name, locker.number);
    }

    for locker in &lockers {
        match &locker.number {
            Some(num) => println!("{}: {}", locker.name, num),
            None => println!("{}: No number", locker.name),
        }
    }
}