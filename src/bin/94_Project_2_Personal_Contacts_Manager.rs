use std::collections::HashMap;
use std::io::{BufRead, Read};
use std::path::PathBuf;
use chrono::format::parse;

struct Record {
    record_num: i64,
    name: String,
    email: String,
}

struct Records {
    inner: HashMap<i64, Record>,
}

impl Records {
    fn new() -> Self {
        Self {
            inner: HashMap::new(),
        }
    }

    fn add_record(&mut self, record: Record) {
        self.inner.insert(record.record_num, record);
    }

    fn into_vec(mut self) -> Vec<Record> {
        let mut records: vec<_> = self.inner.drain().map(|(kv)| kv.1).collect();
        records.sort_by(|rec| rec.record_num);
        return records;
    }
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("invalid record number: {0}")]
    InvalidRecordNumber(#[from] std::num::ParseIntError),
    #[error("Empty record")]
    EmptyRecord,
    #[error("Missing field: {0}")]
    MissingField(String),
}

fn parse_record(record: &str) -> Result<Record, ParseError> {
    let vec: Vec<&str> = record.split(',').collect();
    let record_num = match vec.get(0) {
        Some(record_num) => i64::from_str_radix(record_num, 10)?,
        None => return Err(ParseError::EmptyRecord),
    };
    let name = match vec.get(1).filter(|name| **name != "") {
        Some(name) => name.to_string(),
        None => return Err(ParseError::MissingField("name".to_owned())),
    };
    // let email = vec
    //     .get(2)
    //     .map(|email| email.to_string())
    //     .filter(|email| email != "");

    let email = match vec.get(2).filter(|email| **email != "") {
        Some(email) => email.to_string(),
        None => return Err(ParseError::MissingField("email".to_owned())),
    };

    Ok(Record { record_num, name, email })
}

fn parse_records(records: String, verbose: bool) -> Records {
    let mut new_records = Records::new();
    for (num, record) in records.split('\n').enumerate() {
        if record != "" {
            match parse_record(record) {
                Ok(new_record) => {
                    new_records.add_record(new_record);
                }
                Err(e) => {
                    if verbose {
                        println!("Error parsing record {:?}: {:?}\n > \"{:?}\"\n", num, e, record);
                    }
                }
            }
        }
    }
    return new_records;
}

fn load_records(file_name: PathBuf, verbose: bool) -> std::io::Result<Records> {
    let mut file = std::fs::File::open(file_name)?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    return Ok(parse_records(buffer, verbose));
}

#[derive(StrucOpt, Debug)]
#[structopt(about = "A simple contact manager")]
struct Opt {
    #[structopt(short, parse(from_os_str), default_value = "p2_contacts.csv")]
    data_file: PathBuf,
    #[structopt(subcommand)]
    cmd: Command,
    #[structopt(short, help = "verbose")]
    verbose: bool,
}

#[derive(StructOpt, Debug)]
enum Command {
    List {},
}

fn run(opt: Opt) -> Result<(), std::io::Error> {
    match opt.cmd {
        Command::List { .. } => {
            let records = load_records(opt.data_file, opt.verbose)?;
            for record in records.into_vec() {
                println!("{}, {}, {}", record.record_num, record.name, record.email);
            }
        }
    }
    return Ok(());
}


fn main() {
    let opt = Opt::from_args();
    if let Err(e) = run(opt) {
        eprintln!("The error is {}", e);
    }
}