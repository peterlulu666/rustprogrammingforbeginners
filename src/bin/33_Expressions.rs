enum Menu {
    Burger,
    Fries,
    ApplePie,
}

fn main() {
    let paid = true;
    let item = Menu::Burger;
    let drink = "Water";
    let order_place = match item {
        Menu::Burger => {
            match drink {
                "Water" => "Burger and Water",
                _ => "Burger and Coke",
            }
        }
        _ => "No Burger",
    };
    println!("{}", order_place);
}