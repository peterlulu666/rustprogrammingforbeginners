#[derive(Debug)]
enum TicketLevel {
    Backstage(String),
    VIP(String),
    Standard,
}

#[derive(Debug)]
struct Ticket {
    level: TicketLevel,
    price: f64,
}

fn main() {
    let ticket = vec![
        Ticket {
            level: TicketLevel::Backstage("Peter".to_string()),
            price: 100.0,
        },
        Ticket {
            level: TicketLevel::VIP("John".to_string()),
            price: 50.0,
        },
        Ticket {
            level: TicketLevel::Standard,
            price: 20.0,
        }, ];

    for t in &ticket {
        println!("Ticket: {:?}", t);
    }

    for t in &ticket {
        match t.level {
            TicketLevel::Backstage(ref name) => println!("Backstage: {}, price {}", name, t.price),
            TicketLevel::VIP(ref name) => println!("VIP: {}, price {}", name, t.price),
            TicketLevel::Standard => println!("Standard: price {}", t.price),
        }
    }
}