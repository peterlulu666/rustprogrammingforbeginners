use humantime::format_duration;
use std::time::Duration;
fn main() {
    // add dependencies humantime to Cargo.toml
    let duration = Duration::from_secs(123456789);
    println!("{}", format_duration(duration));
}