use chrono::prelude::*;
use std::time::Duration;
fn main() {
    let local: DateTime<Local> = Local::now();
    println!("Local: {}", local.format("%Y-%m-%d %H:%M:%S").to_string());
}