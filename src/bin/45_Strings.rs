struct Employee {
    name: String,
}

fn main() {
    let emp_name = "John".to_string();
    let emp_name = String::from("John");
    let emp = Employee {
        name: emp_name.clone(),
    };
    println!("Employee name: {}", emp.name);
    println!("Employee name: {}", emp_name);
}