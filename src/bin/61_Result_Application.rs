struct Customer {
    age: i32,
}

fn check_customer_restriction(customer: &Customer) -> Result<(), String> {
    if customer.age < 21 {
        return Err("Customer is not old enough".to_string());
    }
    return Ok(());
    // match customer.age {
    //     0...21 => Err("Customer is under 21".to_string()),
    //     _ => Ok(()),
    // }
}

fn main() {
    let customer = Customer { age: 22 };
    match check_customer_restriction(&customer) {
        Ok(_) => println!("Customer is old enough"),
        Err(e) => println!("{}", e),
    }
}