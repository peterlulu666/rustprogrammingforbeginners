fn create_tuple(a: i32, b: i32) -> (i32, i32) {
    return (a, b);
}

fn main() {
    let coord = create_tuple(1, 2);
    let x = coord.0;
    let y = coord.1;
    if y < 5 {
        println!("y is less than 5");
    } else if y > 5 {
        println!("y is greater than 5");
    } else {
        println!("y is equal to 5");
    }
}