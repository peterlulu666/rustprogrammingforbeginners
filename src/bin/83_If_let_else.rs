enum Color {
    Red,
    Green,
    Blue,
}

fn main() {
    let name = Some("Peter");
    if let Some(name) = name {
        println!("Hello, {:?}!", name);
    } else {
        println!("No name!");
    }

    let color = Color::Red;
    if let Color::Red = color {
        println!("Red!");
    } else {
        println!("Other color!");
    }
}