use std::io;

struct Bill {
    name: String,
    amount: f64,
}

struct Bills {
    bills: Vec<Bill>,
}

impl Bills {
    fn new() -> Bills {
        Bills { bills: Vec::new() }
    }

    fn add_bill(&mut self, bill: Bill) {
        self.bills.push(bill);
    }

    // fn user_input_amount(&self) -> f64 {
    //     let mut input = String::new();
    //     loop {
    //         let amount = user_input();
    //         match amount.parse::<f64>() {
    //             Ok(amount) => return amount,
    //             Err(_) => println!("Please enter a valid number"),
    //         }
    //     }
    // }
    //
    //
    // fn add_bill_menu(&mut self) {
    //     println!("Enter name: ");
    //     let name = user_input();
    //     println!("Enter amount: ");
    //     let amount = self.user_input_amount();
    //     let bill = Bill { name, amount };
    //     self.add_bill(bill);
    // }
    //
    // fn view_bills(&self) {
    //     for bill in &self.bills {
    //         println!("{}: {}", bill.name, bill.amount);
    //     }
    // }
}

fn user_input_amount() -> f64 {
    let mut input = String::new();
    loop {
        let amount = user_input();
        match amount.parse::<f64>() {
            Ok(amount) => return amount,
            Err(_) => println!("Please enter a valid number"),
        }
    }
}

fn add_bill_menu(bills: &mut Bills) {
    println!("Enter name: ");
    let name = user_input();
    println!("Enter amount: ");
    let amount = user_input_amount();
    let bill = Bill { name, amount };
    bills.add_bill(bill);
}

fn view_bills(bills: &Bills) {
    for bill in &bills.bills {
        println!("{}: {}", bill.name, bill.amount);
    }
}

fn user_input() -> String {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read line");
    return input.trim_end().to_string();
}

fn main() {
    let mut bills = Bills { bills: Vec::new() };
    loop {
        println!("1. Add bill");
        println!("2. View bills");
        println!("3. Exit");
        let input = user_input();
        // match input.as_str() {
        //     "1" => bills.add_bill_menu(),
        //     "2" => bills.view_bills(),
        //     "3" => break,
        //     _ => println!("Please enter a valid option"),
        // }
        match input.as_str() {
            "1" => add_bill_menu(&mut bills),
            "2" => view_bills(&bills),
            "3" => break,
            _ => println!("Please enter a valid option"),
        }
    }
}