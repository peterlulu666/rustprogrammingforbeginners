enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn main() {
    let go = Direction::Up;
    match go {
        Direction::Up => println!("Going up"),
        Direction::Down => println!("Going down"),
        Direction::Left => println!("Going left"),
        Direction::Right => println!("Going right"),
    }
}