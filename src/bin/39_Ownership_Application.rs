struct Item {
    quantity: i32,
    item_num: i32,
}

fn print_quantity(item: &Item) {
    println!("quantity = {}", item.quantity);
}

fn print_item_num(item: &Item) {
    println!("item_num = {}", item.item_num);
}

fn main() {
    let item = Item {
        quantity: 100,
        item_num: 10,
    };
    print_quantity(&item);
    print_item_num(&item);
}