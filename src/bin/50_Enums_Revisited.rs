#[derive(Debug)]
enum Discount {
    User,
    Percent(i32),
}

fn main() {
    let discount = Discount::Percent(10);
    println!("discount = {:?}", discount);
}