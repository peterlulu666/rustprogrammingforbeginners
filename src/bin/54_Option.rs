struct Survey {
    question1: Option<i32>,
    question2: Option<bool>,
    question3: Option<String>,
}

fn main() {
    let survey1 = Survey {
        question1: None,
        question2: None,
        question3: None,
    };

    match survey1.question1 {
        Some(answer) => println!("question1 = {}", answer),
        None => println!("question1 = None"),
    }
    match survey1.question2 {
        Some(answer) => println!("question1 = {}", answer),
        None => println!("question1 = None"),
    }

    match survey1.question3 {
        Some(answer) => println!("question1 = {}", answer),
        None => println!("question1 = None"),
    }

    let survey2 = Survey {
        question1: Some(1),
        question2: Some(true),
        question3: Some("hello".to_string()),
    };

    match survey2.question1 {
        Some(answer) => println!("question1 = {}", answer),
        None => println!("question1 = None"),
    }
    match survey2.question2 {
        Some(answer) => println!("question1 = {}", answer),
        None => println!("question1 = None"),
    }

    match survey2.question3 {
        Some(answer) => println!("question1 = {}", answer),
        None => println!("question1 = None"),
    }
}