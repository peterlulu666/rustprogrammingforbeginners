enum Flavor {
    Chocolate,
    Vanilla,
    Strawberry,
}

struct Drink {
    flavor: Flavor,
    oz: i32,
}

fn print_drink(drink: &Drink) {
    match drink.flavor {
        Flavor::Chocolate => println!("Chocolate"),
        Flavor::Vanilla => println!("Vanilla"),
        Flavor::Strawberry => println!("Strawberry"),
    }
    println!("{} oz of ", drink.oz);
}

fn main() {
    let chocolate = Drink {
        flavor: Flavor::Chocolate,
        oz: 10,
    };
    print_drink(&chocolate);
}