struct Scores {
    score: i32,
}

fn main() {
    let scores = vec![
        Scores { score: 1 },
        Scores { score: 2 },
        Scores { score: 3 }];
    for score in scores {
        println!("score = {}", score.score);
    }
}