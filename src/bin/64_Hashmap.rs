use std::collections::HashMap;

struct Contents {
    content: String,
}

fn main() {
    let mut locker = HashMap::new();
    locker.insert(1, Contents {content: "Cloth".to_string()});
    locker.insert(2, Contents {content: "Shoes".to_string()});
    locker.insert(3, Contents {content: "Books".to_string()});
    for (number, contents) in &locker {
        println!("Locker {} contains {}", number, contents.content);
    }
}