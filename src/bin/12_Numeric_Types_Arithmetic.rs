fn substraction(x: i32, y: i32) -> i32 {
    return x - y;
}

fn main() {
    println!("{:?}", 1 + 1);
    println!("{:?}", 10 - 1);
    println!("{:?}", 2 * 3);
    println!("{:?}", 19 / 9);
    println!("{:?}", 19 % 9);
    println!("{:?}",substraction(100, 10));
}