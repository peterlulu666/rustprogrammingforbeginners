struct Temperature {
    degrees: f64,
}

impl Temperature {
    fn new(degrees: f64) -> Self {
        Self { degrees: degrees }
    }

    fn hot_temperature(&self) {
        println!("It's hot!");
    }
}

// struct Temperature (f64);
//
// impl Temperature {
//     fn new(degrees: f64) -> Self {
//         Self (degrees)
//     }
//
//     fn hot_temperature(&self) {
//         println!("It's hot!");
//     }
// }

fn main() {
    let temp = Temperature::new(100.0);
    println!("Temperature: {} degrees", temp.degrees);
    temp.hot_temperature();
    // let temp = Temperature::new(100.0);
    // println!("Temperature: {}", temp.0);
    // temp.hot_temperature();
}