#[derive(Debug)]
struct Package {
    weight: i32,
}

impl Package {
    fn new(weight: i32) -> Self {
        Self { weight: weight }
    }
}

impl Default for Package {
    fn default() -> Self {
        Self { weight: 100 }
    }
}

fn main() {
    let p1 = Package::new(10);
    println!("p1 = {:?}", p1);
    let p2 = Package::default();
    println!("p2 = {:?}", p2);
}