fn main() {
    for i in 1..10 {
        println!("{}", i);
    }

    for i in 1..=10 {
        println!("{}", i);
    }

    for i in 'a'..='z' {
        println!("{:?}", i);
    }

}