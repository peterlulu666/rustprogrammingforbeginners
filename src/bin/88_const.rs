const MAX_SPEED: i32 = 200;
fn clap_speed(speed: i32) -> i32 {
    if speed > MAX_SPEED {
        MAX_SPEED
    } else {
        speed
    }
}

fn main() {
    let speed = 300;
    println!("clap_speed({}) = {}", speed, clap_speed(speed));
    let speed = 100;
    println!("clap_speed({}) = {}", speed, clap_speed(speed));
}