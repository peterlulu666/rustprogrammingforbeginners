enum Discount {
    Percentage(i32),
    Fixed(i32),
}

struct Ticket {
    price: i32,
    event: String,
}

fn main() {
    let a = 3;
    match a {
        1 => println!("a is 1"),
        2 => println!("a is 2"),
        3 => println!("a is 3"),
        _ => println!("a is not 1, 2, or 3"),
    }

    match a {
        1 => println!("a is 1"),
        2 => println!("a is 2"),
        3 => println!("a is 3"),
        other => println!("a is other {}", other),
    }

    let discount = Discount::Percentage(10);
    match discount {
        Discount::Percentage(10) => println!("10% discount"),
        _ => println!("No discount"),
    }

    let ticket = Ticket {
        price: 1000,
        event: "Concert".to_string(),
    };

    match &ticket {
        Ticket { price, event } => println!("price is {}", price),
    }
    match &ticket {
        Ticket { price: 100, event } => println!("{} is $100", event),
        _ => println!("No discount"),
    }

    match &ticket {
        Ticket { price: 100, event } => println!("{} is $100", event),
        Ticket { price, event } => println!("price is {}", price),
    }
}