#[derive(Debug)]
enum Color {
    Black,
    Blue,
    Brown,
    CustomColor(String),
    Green,
    Red,
    White,
    Yellow,
}

#[derive(Debug)]
struct ShirtColor(Color);

impl ShirtColor {
    fn new(color: Color) -> Result<Self, String> {
        match color {
            Color::White => Err("White is not a selection ".to_owned()),
            _ => Ok(Self(color)),
        }
    }
}

#[derive(Debug)]
struct ShoeColor(Color);

impl ShoeColor {
    fn new(color: Color) -> Self {
        Self(color)
    }
}

#[derive(Debug)]
struct PantsColor(Color);

impl PantsColor {
    fn new(color: Color) -> Self {
        Self(color)
    }
}

fn print_shirt_color(shirt_color: Result<ShirtColor, String>) {
    println!("Shirt color: {:?}", shirt_color);
}

fn print_shoe_color(shoe_color: ShoeColor) {
    println!("Shoe color: {:?}", shoe_color);
}

fn print_pants_color(pants_color: PantsColor) {
    println!("Pants color: {:?}", pants_color);
}

fn main() {
    let shirt_color = ShirtColor::new(Color::White);
    let shoe_color = ShoeColor::new(Color::Blue);
    let pants_color = PantsColor::new(Color::Brown);
    print_shirt_color(shirt_color);
    print_shoe_color(shoe_color);
    print_pants_color(pants_color);
}