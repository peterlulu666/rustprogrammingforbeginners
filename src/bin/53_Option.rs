#[derive(Debug)]
struct Item {
    name: String,
    quantity: i32,
}

fn get_item(name: &str) -> Option<Item> {
    return match name {
        "apple" => Some(Item { name: "apple".to_string(), quantity: 1 }),
        "banana" => Some(Item { name: "banana".to_string(), quantity: 2 }),
        "orange" => Some(Item { name: "orange".to_string(), quantity: 3 }),
        _ => None,
    };
}

fn main() {
    let item = "apple";
    println!("{:?}",get_item(&item));
}