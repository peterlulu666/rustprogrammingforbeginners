use std::collections::HashMap;

fn main() {
    let mut people = HashMap::new();
    people.insert("Wang", 18);
    people.insert("Li", 20);
    people.insert("Tan", 22);

    // Search
    match people.get("Wang") {
        Some(age) => println!("Wang is {} years old", age),
        None => println!("Wang is not in the map"),
    }

    // iterate
    for (name, age) in people.iter() {
        println!("{} is {} years old", name, age);
    }

    for name in people.keys() {
        println!("{}", name);
    }

    for age in people.values() {
        println!("{}", age);
    }

}