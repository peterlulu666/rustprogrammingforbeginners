fn main() {
    let mut num = Some(3);
    while let Some(i) = num {
        println!("num: {}", i);
        num = None;
    }

    let mut v = vec![1, 2, 3];
    let mut v_iter = v.iter();
    while let Some(i) = &v_iter.next() {
        println!("v_iter: {}", i);
    }
}