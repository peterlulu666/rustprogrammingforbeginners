struct Box {
    width: i32,
    height: i32,
    depth: i32,
}

fn main() {
    let b = Box {
        width: 10,
        height: 20,
        depth: 30,
    };
    let height = b.height;
    println!("height = {}", height);
}