fn main() {
    let a: Option<i32> = Some(1);
    println!("{:?}", a);
    println!("{}", a.is_some());
    println!("{}", a.is_none());
    println!("{:?}", a.map(|x| x + 1));
    println!("{:?}", a.filter(|x| x == &1));
    println!("{:?}", a.or_else(|| Some(2)));
    println!("{:?}", a.and_then(|x| Some(x + 1)));
    println!("{:?}", a.unwrap_or(2));
    println!("{:?}", a.unwrap_or_else(|| 2));
}