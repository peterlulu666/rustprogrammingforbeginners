#[derive(Debug)]
enum Access {
    Full,
}

fn one_two_three() -> (i32, i32, i32) {
    (1, 2, 3)
}

fn main() {
    let number = one_two_three();
    let (a, b, c) = one_two_three();
    println!("{:?} {:?}", a, number.0);
    println!("{:?} {:?}", b, number.1);
    println!("{:?} {:?}", c, number.2);

    let (employee,access) = ("Peter", Access::Full);
    println!("{:?} {:?}", employee, access);
}