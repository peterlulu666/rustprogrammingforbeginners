# [derive(Debug, Clone, Copy)]
enum Position {
    Manager,
    Developer,
    Tester,
}

#[derive(Debug, Clone, Copy)]
struct Employee {
    age: i64,
    position: Position,
}

fn print_employee(employee: Employee) {
    println!("{} is a {:?}", employee.age, employee.position);
}

fn main() {
    let e = Employee {
        age: 10,
        position: Position::Manager,
    };
    print_employee(e);
    print_employee(e);
}