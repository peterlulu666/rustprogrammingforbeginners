fn main() {
    let add = |x: i32, y: i32| -> i32 { x + y };
    let plus = |a: i32, b: i32| a + b;
    println!("add(1, 2) = {}", add(1, 2));
    println!("plus(1, 2) = {}", plus(1, 2));

}