struct Item {
    name: String,
    quantity: i32,
}

fn print_name(name: &str) {
    println!("name = {}", name);
}

fn main() {
    let item = vec![
        Item {
            name: "apple".to_string(),
            quantity: 1,
        },
        Item {
            name: "banana".to_string(),
            quantity: 2,
        },
        Item {
            name: "orange".to_string(),
            quantity: 3,
        },
    ];
    for item in item {
        print_name(&item.name);
        println!("quantity = {}", item.quantity);
    }
}