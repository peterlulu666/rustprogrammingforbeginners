struct User {
    user_num: i32,
    user_name: String,
}

fn finder_user(name: &str) -> Option<i32> {
    return match name {
        "Wang" => Some(1),
        "Li" => Some(2),
        "Tan" => Some(3),
        _ => None,
    };
}

fn main() {
    let user = "Wang";
    let user_num = finder_user(user).map(
        |num| User {
            user_num: num,
            user_name: user.to_string(),
        }
    );
    for user in user_num {
        println!("{}: {}", user.user_num, user.user_name);
    }
}