mod greeting {
    pub(crate) fn hello() {
        println!("Hello");
    }

    pub(crate) fn goodbye() {
        println!("Goodbye");
    }
}

mod math {
    pub(crate) fn add() {
        println!("Add");
    }

    pub(crate) fn subtract() {
        println!("Subtract");
    }
}

fn main() {
    greeting::hello();
    greeting::goodbye();
    math::add();
    math::subtract();
}