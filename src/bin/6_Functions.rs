fn plus(a: i32, b: i32) -> i32 {
    return a + b;
}

fn main() {
    println!("plus(1, 2) = {}", plus(1, 2));
}