enum EmployeeType {
    Maintainer,
    Marketing,
    Manager,
    LineManager,
    KitchenStaff,
    AssemblyTechnician,
}

struct Employee {
    name: String,
    employee_type: EmployeeType,
    status: bool,
}

fn check_status(employee: &Employee) -> Result<(), String> {
    if employee.status == false {
        return Err("Employee status is not active".to_string());
    }
    return Ok(());
}

fn check_access(employee: &Employee) -> Result<(), String> {
    check_status(employee)?;
    match employee.employee_type {
        EmployeeType::Maintainer => Ok(()),
        EmployeeType::Marketing => Ok(()),
        EmployeeType::Manager => Ok(()),
        _ => Err("Employee does not have access".to_string()),
    }
}

fn main() {
    let e = Employee {
        name: "Wang".to_string(),
        employee_type: EmployeeType::Manager,
        status: true,
    };
    match check_access(&e) {
        Ok(_) => println!("Access granted"),
        Err(e) => println!("{}", e),
    }
}