use std::collections::HashMap;

fn main() {
    let mut furniture = HashMap::new();
    furniture.insert("chair", 5);
    furniture.insert("bed", 3);
    furniture.insert("table", 2);
    furniture.insert("couch", 0);
    let mut total = 0;
    for (item, count) in &furniture {
        total = total + count;
        if *count == 0 {
            println!("{} is out of stock", item);
        } else {
            println!("{}: {}", item, count);
        }
    }
    println!("total is {}", total);
}