struct Color {
    color: String,
}

impl Color {
    fn new(color: String) -> Self {
        Self { color: color }
    }

    fn print_color(&self) {
        println!("Color: {}", self.color);
    }
}

struct Dimensions {
    length: f64,
    width: f64,
    height: f64,
}

impl Dimensions {
    fn new(length: f64, width: f64, height: f64) -> Self {
        Self { length: length, width: width, height: height }
    }

    fn print_dimensions(&self) {
        println!("Dimensions: {} x {} x {}", self.length, self.width, self.height);
    }
}

struct Product {
    weight: f64,
    color: Color,
    dimensions: Dimensions,
}

impl Product {
    fn new(weight: f64, color: Color, dimensions: Dimensions) -> Self {
        Self { weight: weight, color: color, dimensions: dimensions }
    }

    fn print_product(&self) {
        self.color.print_color();
        self.dimensions.print_dimensions();
        println!("Weight: {}", self.weight);
    }
}

fn main() {
    let color = Color::new(String::from("Red"));
    let dimensions = Dimensions::new(10.0, 20.0, 30.0);
    let product = Product::new(100.0, color, dimensions);
    product.print_product();
}