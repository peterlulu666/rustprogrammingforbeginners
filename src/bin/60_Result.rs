#[derive(Debug)]
enum MenuChoice {
    MainMenu,
    Start,
    Quit,
}

fn get_choice(input: &str) -> Result<MenuChoice, String> {
    match input {
        "mainmenu" => Ok(MenuChoice::MainMenu),
        "start" => Ok(MenuChoice::Start),
        "quit" => Ok(MenuChoice::Quit),
        _ => Err(format!("Invalid choice: {}", input)),
    }
}

fn print_choices(choice: &MenuChoice) {
    println!("Choices: {:?}", choice);
}

fn pick_choice(input: &str) -> Result<(), String> {
    let choice = get_choice(input)?;
    print_choices(&choice);
    Ok(())
}

fn main() {
    let choice = pick_choice("start");
    println!("choice = {:?}", choice);
}