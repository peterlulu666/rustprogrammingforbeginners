fn main() {
    let v = vec![10, 20, 30, 40];
    for i in &v {
        // if i == 30 {
        //     println!("thirty");
        // } else {
        //     println!("{}", i);
        // }
        match i {
            30 => println!("thirty"),
            _ => println!("{}", i),
        }
    }
    println!("length = {}", v.len());
}