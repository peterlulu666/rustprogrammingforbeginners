fn plus(a: i64, b: i64) -> i64 {
    return a + b;
}

fn print_plus(a: i64) {
    println!("{:?}", a);
}

fn main() {
    let a = plus(1, 2);
    print_plus(a);
}