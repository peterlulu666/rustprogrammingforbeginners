struct Book {
    pages: i32,
    rating: i32,
}

fn print_pages(book: &Book) {
    println!("Pages: {}", book.pages);
}

fn print_rating(book: &Book) {
    println!("Rating: {}", book.rating);
}
fn main() {
    let book = Book { pages: 100, rating: 10 };
    print_pages(&book);
    print_rating(&book);
}