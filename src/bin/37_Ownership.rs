enum Light {
    Red,
    Yellow,
    Green,
}

// fn print_light(light: Light) {
//     match light {
//         Light::Red => println!("Red"),
//         Light::Yellow => println!("Yellow"),
//         Light::Green => println!("Green"),
//     }
// }

// fn print_light(light: &Light) {
//     match light {
//         Light::Red => println!("Red"),
//         Light::Yellow => println!("Yellow"),
//         Light::Green => println!("Green"),
//     }
// }
fn main() {
    let yellow = Light::Yellow;
    // print_light(yellow);
    // print_light(yellow);
    print_light(&yellow);
    print_light(&yellow);
}