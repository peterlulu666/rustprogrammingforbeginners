fn maybe_num() -> Option<i32> {
    Some(5)
}

fn maybe_str() -> Option<String> {
    Some("hello".to_string())
}

fn main() {
    let num = match maybe_num() {
        Some(n) => Some(n + 1),
        None => None,
    };
    println!("num = {:?}", num);

    let str = match maybe_str() {
        Some(s) => Some(s),
        None => None,
    };
    println!("str = {:?}", str);

    let num_map = maybe_num().map(|n| n + 1);
    println!("num_map = {:?}", num_map);
}