use std::io;

fn user_input() -> io::Result<String> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    return Ok(input.trim().to_string());
}

fn main() {
    let mut all_input = vec![];
    let mut times_input = 0;
    while times_input < 2 {
        let input = user_input();
        match input {
            Ok(content) => {
                all_input.push(content);
                times_input += 1;
            }
            Err(_) => println!("Failed to read line"),
        }
    }
    for input in all_input {
        println!("content is {}, capitalize content is {}", input, input.to_uppercase());
    }
    // while times_input < 2 {
    //     let input = user_input();
    //     match input {
    //         (content) => {
    //             all_input.push(content);
    //             times_input += 1;
    //         }
    //         (_) => println!("Failed to read line"),
    //     }
    // }
    // for input in all_input {
    //     println!("content is {}, capitalize content is {}", input, input.to_uppercase());
    // }
}

// fn user_input() -> String {
//     let mut input = String::new();
//     io::stdin().read_line(&mut input).expect("Failed to read line");
//     return input.trim().to_string();
// }


