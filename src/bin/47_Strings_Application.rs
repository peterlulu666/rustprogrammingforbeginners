struct Person {
    name: String,
    color: String,
    age: i32,
}

fn print_info(info: &str) {
    println!("{}", info);
}

fn main() {
    let person = vec![
        Person {
            name: "Wang".to_string(),
            color: "Red".to_string(),
            age: 8,
        },
        Person {
            name: "Li".to_string(),
            color: "Blue".to_string(),
            age: 9,
        },
        Person {
            name: "Tan".to_string(),
            color: "Green".to_string(),
            age: 19,
        },
    ];
    for p in person {
        if p.age < 10 {
            print_info(&p.name);
            print_info(&p.color);
        }
    }
}