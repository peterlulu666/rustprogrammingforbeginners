fn print_wt(wt: bool) {
    match wt {
        true => println!("big"),
        false => println!("small"),
    }
}

fn main() {
    let value = 100;
    let wt = value > 100;
    print_wt(wt);
}