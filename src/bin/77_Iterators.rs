fn main() {
    let v = vec![1, 2, 3, 4, 5];

    let mut plus_one = vec![];
    for num in &v {
        plus_one.push(num + 1);
    }
    println!("plus_one = {:?}", plus_one);

    for num in v.iter() {
        plus_one.push(num + 1);
    }
    println!("plus_one = {:?}", plus_one);

    let plus_one_iter: Vec<_> = v.iter().map(|x| x + 1).collect();
    println!("plus_one_iter = {:?}", plus_one_iter);

    let filter_vec = v.iter().filter(|x| x > &&2).collect::<Vec<_>>();
    println!("filter_vec = {:?}", filter_vec);

    let find_vec = v.iter().find(|x| x > &&2);
    println!("find_vec = {:?}", find_vec);

    let count = v.iter().count();
    println!("count = {:?}", count);

    let last = v.iter().last();
    println!("last = {:?}", last);

    let min = v.iter().min();
    println!("min = {:?}", min);

    let max = v.iter().max();
    println!("max = {:?}", max);

    let take = v.iter().take(2).collect::<Vec<_>>();
    println!("take = {:?}", take);
}