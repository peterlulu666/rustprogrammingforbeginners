struct Item {
    quantity: i32,
    price: f32,
}

fn main() {
    let i = Item {
        quantity: 10,
        price: 9.99,
    };
    println!("i.quantity = {}", i.quantity);
    println!("i.price = {}", i.price);
}