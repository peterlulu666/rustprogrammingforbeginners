trait Noise {
    fn make_noise(&self);
}

struct Person;

struct Dog;

impl Noise for Person {
    fn make_noise(&self) {
        println!("Hello");
    }
}

impl Noise for Dog {
    fn make_noise(&self) {
        println!("Woof");
    }
}

fn print_noise(noise_maker: &impl Noise) {
    noise_maker.make_noise();
}

fn main() {
    let person = Person;
    let dog = Dog;
    print_noise(&person);
    print_noise(&dog);
}