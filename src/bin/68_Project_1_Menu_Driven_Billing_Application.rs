use std::collections::HashMap;
use std::io;

fn user_input() -> String {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read line");
    return input.trim_end().to_string();
}

fn create_bill(bill: &mut HashMap<String, String>) {
    let mut name = String::new();
    let mut amount = String::new();
    println!("enter name: ");
    name = user_input();
    println!("enter amount: ");
    amount = user_input();
    bill.insert(name, amount);
}

fn view_bill(bill: &mut HashMap<String, String>) {
    for (name, amount) in bill {
        println!("{}: {}", name, amount);
    }
    println!("----------------");
}

fn remove_bill(bill: &mut HashMap<String, String>) {
    let mut name = String::new();
    println!("enter name: ");
    name = user_input();
    bill.remove(&name);
}

fn edit_bill(bill: &mut HashMap<String, String>) {
    loop {
        println!("enter name: ");
        let name = user_input();
        if bill.contains_key(&name) {
            println!("enter amount: ");
            let amount = user_input();
            bill.insert(name, amount);
            break;
        } else {
            println!("name does not exist");
        }
    }
}

fn main() {
    let mut bill = HashMap::new();
    loop {
        println!("1. Create bill");
        println!("2. View bill");
        println!("3. Remove bill");
        println!("4. Edit bill");
        println!("5. Exit");
        println!("Enter your choice: ");
        let choice = user_input();
        match choice.as_str() {
            "1" => create_bill(&mut bill),
            "2" => view_bill(&mut bill),
            "3" => remove_bill(&mut bill),
            "4" => edit_bill(&mut bill),
            "5" => break,
            _ => println!("invalid choice"),
        }
    }
}