enum AccessLevel {
    Read,
    Write,
    Admin,
}

fn main() {
    let access_level = AccessLevel::Admin;
    let can_access = match access_level {
        AccessLevel::Admin => true,
        _ => false,
    };
    println!("can_access = {}", can_access);
}