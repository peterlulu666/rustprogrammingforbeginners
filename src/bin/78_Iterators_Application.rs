fn main() {
    let data = vec![1, 2, 3, 4, 5];
    // let num_triple:Vec<_> = data.iter().map(|x| x * 3).collect();
    // println!("num_triple = {:?}", num_triple);
    // let num_filter:Vec<_> = num_triple.iter().filter(|x| x > &&10).collect();
    // println!("num_filter = {:?}", num_filter);
    // for x in num_filter.iter() {
    //     println!("x = {}", x);
    // }

    let num_filter:Vec<_> = data.iter().map(|x| x * 3).filter(|x| x > &10).collect();
    println!("num_filter = {:?}", num_filter);
}